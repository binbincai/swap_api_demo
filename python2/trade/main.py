#coding: utf-8

# python2.7 main.py
import urllib
import json
import base64
import hmac, hashlib
import time

# test userId: 850982115493519362
APIURL = "https://pre-swap-api.yunzhonghui.vip"
APIKEY = "AdyxLBCpU6l1iBi4puZMerbu8ZRhpar1dM9iQnMDotkaVImW"
SECRETKEY = "BBIX68aoTPbXFjHzyZS1DDZaQJZhByLC7tcQSa3mDgVl6QoIkD1u2yg1KVVdzjCV"

def genSignature(paramsMap):
	sortedKeys = sorted(paramsMap)
	paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
	return hmac.new(SECRETKEY.encode("utf-8"), paramsStr.encode("utf-8"), digestmod=hashlib.sha256).digest()

def post(url, body):
	return urllib.urlopen(url, body.encode("utf-8")).read()

def placeOrder(symbol, side, price, volume, tradeType, action):
	paramsMap = {
		"symbol": symbol,
		"apiKey": APIKEY,
		"side": side,
		"entrustPrice": price,
		"entrustVolume": volume,
		"tradeType": tradeType,
		"action": action,
		"timestamp": int(time.time()),
	}
	sortedKeys = sorted(paramsMap)
	paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
	paramsStr += "&sign=" + urllib.quote(base64.b64encode(genSignature(paramsMap)))
	url = "%s/api/v1/user/trade" % APIURL
	return post(url, paramsStr)

def queryOrders(symbol):
	paramsMap = {
		"symbol": symbol,
		"apiKey": APIKEY,
		"timestamp": int(time.time()),
	}
	sortedKeys = sorted(paramsMap)
	paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
	paramsStr += "&sign=" + urllib.quote(base64.b64encode(genSignature(paramsMap)))
	url = "%s/api/v1/user/pendingOrders" % APIURL
	return post(url, paramsStr)

def cancelOrder(symbol, orderId):
	paramsMap = {
		"symbol": symbol,
		"apiKey": APIKEY,
		"orderId": orderId,
		"timestamp": int(time.time()),
	}
	sortedKeys = sorted(paramsMap)
	paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
	paramsStr += "&sign=" + urllib.quote(base64.b64encode(genSignature(paramsMap)))
	url = "%s/api/v1/user/cancelOrder" % APIURL
	return post(url, paramsStr)

def main():
	symbol = "BTC-USDT"
	side = "Bid"
	price = 10000 # 10000USDT
	volume = 0.01 # 0.01BTC
	tradeType = "Limit"
	action = "Open"
	placeResp = placeOrder(symbol, side, price, volume, tradeType, action)
	placeResp = json.loads(placeResp)
	if placeResp["code"] != 0:
		print("placeOrder fail, err:", placeResp["msg"])	
		return
	print("placeOrder success, orderId:", placeResp["data"]["orderId"])

	queryResp = queryOrders(symbol)
	queryResp = json.loads(queryResp)
	if queryResp["code"] != 0:
		print("queryOrders fail, err:", queryResp["msg"])
		return
	print("queryOrders success, orders:", json.dumps(queryResp))

	orderIds = [x["orderId"] for x in queryResp["data"]["orders"]]
	for orderId in orderIds:
		cancelResp = cancelOrder(symbol, orderId)
		cancelResp = json.loads(cancelResp)
		if cancelResp["code"] != 0:
			print("cancelOrder fail, err:", cancelResp["msg"])
			return
		print("cancelOrder success, orderId:", orderId)
		time.sleep(2)

	"""
	placeOrder success, orderId: 584984548
	queryOrders success, orders: {"code": 0, "msg": "Success", "data": {"orders": [{"orderId": "584984548", "side": "Bid", "action": "Open", "tradeType": "", "entrustVolume": 0.01, "entrustPrice": 10000, "filledVolume": 0, "avgFilledPrice": 0, "entrustTm": "2021-01-23T08:14:03Z", "symbol": "BTC-USDT"}]}}
	cancelOrder success, orderId: 584984548
	"""

if __name__ == "__main__":
	main()
