package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

// test userId: 850982115493519362
var APIUrl = "https://pre-swap-api.yunzhonghui.vip"
var APIKey = "AdyxLBCpU6l1iBi4puZMerbu8ZRhpar1dM9iQnMDotkaVImW"
var SecretKey = "BBIX68aoTPbXFjHzyZS1DDZaQJZhByLC7tcQSa3mDgVl6QoIkD1u2yg1KVVdzjCV"

func mapToURLQuery(mapParams map[string]string) string {
	sortedParamKeys := make([]string, 0)
	for paramKey := range mapParams {
		sortedParamKeys = append(sortedParamKeys, paramKey)
	}
	sort.Strings(sortedParamKeys)

	sortedParams := make([]string, 0)
	for _, paramKey := range sortedParamKeys {
		paramValue := mapParams[paramKey]
		param := paramKey + "=" + paramValue
		sortedParams = append(sortedParams, param)
	}
	return strings.Join(sortedParams, "&")
}

func genSignature(mapParams map[string]string) string {
	h := hmac.New(sha256.New, []byte(SecretKey))
	h.Write([]byte(mapToURLQuery(mapParams)))
	return url.QueryEscape(base64.StdEncoding.EncodeToString(h.Sum(nil)))
}

func post(url, body string) (responseData []byte, err error) {
	mime := "application/x-www-form-urlencoded"
	reader := bytes.NewBufferString(body)
	response, err := http.Post(url, mime, reader)
	if err != nil {
		return
	}
	if response.StatusCode != http.StatusOK {
		err = errors.New(fmt.Sprint("post http fail:", response.StatusCode))
		return
	}
	defer response.Body.Close()
	responseData, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}
	return
}

func placeOrder(symbol, side string, price, volume float64, tradeType, action string) (responseData []byte, err error) {
	mapParams := map[string]string{
		"symbol":        symbol,
		"apiKey":        APIKey,
		"side":          side,
		"entrustPrice":  fmt.Sprint(price),
		"entrustVolume": fmt.Sprint(volume),
		"tradeType":     tradeType,
		"action":        action,
		"timestamp":     fmt.Sprint(time.Now().Unix()),
	}
	signature := genSignature(mapParams)
	url := fmt.Sprintf("%s/api/v1/user/trade", APIUrl)
	params := mapToURLQuery(mapParams) + "&sign=" + signature
	responseData, err = post(url, params)
	return
}

func queryOrders(symbol string) (responseData []byte, err error) {
	mapParams := map[string]string{
		"symbol":    symbol,
		"apiKey":    APIKey,
		"timestamp": fmt.Sprint(time.Now().Unix()),
	}
	signature := genSignature(mapParams)
	url := fmt.Sprintf("%s/api/v1/user/pendingOrders", APIUrl)
	params := mapToURLQuery(mapParams) + "&sign=" + signature
	responseData, err = post(url, params)
	return
}

func cancelOrder(symbol, orderID string) (responseData []byte, err error) {
	mapParams := map[string]string{
		"symbol":    symbol,
		"apiKey":    APIKey,
		"orderId":   orderID,
		"timestamp": fmt.Sprint(time.Now().Unix()),
	}
	signature := genSignature(mapParams)
	url := fmt.Sprintf("%s/api/v1/user/cancelOrder", APIUrl)
	params := mapToURLQuery(mapParams) + "&sign=" + signature
	responseData, err = post(url, params)
	return
}

func main() {
	symbol := "BTC-USDT"
	side := "Bid"
	price := 10000.0 // 10000USDT
	volume := 0.01   // 0.01BTC
	tradeType := "Limit"
	action := "Open"
	placeResponseData, err := placeOrder(symbol, side, price, volume, tradeType, action)
	if err != nil {
		fmt.Println("placeOrder fail, err:", err)
		return
	}
	placeResponse := &struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
		Data struct {
			OrderID string `json:"orderId"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(placeResponseData, placeResponse)
	if err != nil {
		fmt.Println("unmarshal placeOrder response fail, err:", err)
		return
	}
	if placeResponse.Code != 0 {
		fmt.Println("placeOrder return fail, err:", placeResponse.Msg)
		return
	}
	fmt.Println("placeOrder success, orderID:", placeResponse.Data.OrderID)

	queryResponseData, err := queryOrders(symbol)
	if err != nil {
		fmt.Println("queryOrders fail, err:", err)
		return
	}
	queryResponse := &struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
		Data struct {
			Orders []struct {
				OrderID        string  `json:"orderId"`
				Side           string  `json:"side"`
				Action         string  `json:"action"`
				TradeType      string  `json:"tradeType"`
				EntrustVolume  float64 `json:"entrustVolume"`
				EntrustPrice   float64 `json:"entrustPrice"`
				FilledVolume   float64 `json:"filledVolume"`
				AvgFilledPrice float64 `json:"avgFilledPrice"`
				EntrustTm      string  `json:"entrustTm"`
				Symbol         string  `json:"symbol"`
			} `json:"orders"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(queryResponseData, queryResponse)
	if err != nil {
		fmt.Println("unmarshal queryOrders response fail, err:", err)
		return
	}
	if queryResponse.Code != 0 {
		fmt.Println("queryOrders return fail, err:", queryResponse.Msg)
		return
	}
	fmt.Println("queryOrders success, response:", string(queryResponseData))

	for _, order := range queryResponse.Data.Orders {
		cancelResponseData, err := cancelOrder(symbol, order.OrderID)
		if err != nil {
			fmt.Println("cancelOrder fail, err:", err)
			return
		}
		cancelResponse := &struct {
			Code int    `json:"code"`
			Msg  string `json:"msg"`
		}{}
		err = json.Unmarshal(cancelResponseData, cancelResponse)
		if err != nil {
			fmt.Println("unmarshal cancelOrder response fail, err:", err)
			return
		}
		if cancelResponse.Code != 0 {
			fmt.Println("cancelOrder return fail, err:", cancelResponse.Msg)
			return
		}
		fmt.Println("cancelOrder success, orderID:", order.OrderID)
		time.Sleep(5 * time.Second)
	}
}
